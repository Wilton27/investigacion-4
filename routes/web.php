<?php

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login')->name('login.attempt')->uses('Auth\LoginController@login');

Route::group(['middleware' => 'auth'], function () {


    Route::resource('/', 'DashController');
    Route::get('/logout')->name('logout')->uses('Auth\LoginController@logout');

    Route::get('/users', 'UserController@index');

    Route::get('/users/create', 'UserController@create');

    Route::post('/users', 'UserController@store');

    Route::get('/users/{id}/edit', 'UserController@edit');

    Route::put('/users/{id}', 'UserController@update');

    Route::delete('/users/{id}', 'UserController@destroy');

});

